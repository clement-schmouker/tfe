
const THREE = require('three'); //import tous les exports de three, les store dans un objet THREE, depuis le fichier "three"
// var OBJLoader = require('three-obj-loader');
import OBJManager from 'Managers/OBJManager';
import SceneObject from 'entities/SceneObject';

class Room extends THREE.Object3D {

	constructor(){
		super();
		this._scale = 10;
		this._ground;
		this._walls = [];
		this._lights = [];
		this._items = [];
		this._door;
		this._wallDistance = 260;

		this.objectManager = new OBJManager();


	};


	initialize() {
		this.objectManager.onQueueLoaded = function (objects){
			objects.map(function(elem) {
				elem.object.children.map(function(child) {
					child.castShadow = true;
					child.receiveShadow = true;
				})
				// this.add(elem.object);
			}.bind(this))

			this.initContent();
		}.bind(this);
		this.objectManager.loadObjects([
			{
				url: '../../3Dassets/door.obj',
				name: 'door'
			},
			{
				url: '../../3Dassets/shelf.obj',
				name: 'shelf'
			},
			{
				url: '../../3Dassets/switch.obj',
				name: 'switch'
			},
			{
				url: '../../3Dassets/taperecorder.obj',
				name: 'taperecorder'
			},
			{
				url: '../../3Dassets/tape.obj',
				name: 'tape'
			},
			{
				url: '../../3Dassets/table.obj',
				name: 'table'
			},
			{
				url: '../../3Dassets/key.obj',
				name: 'key'
			},
			{
				url: '../../3Dassets/wheels.obj',
				name: 'wheels'
			},
			{
				url: '../../3Dassets/mirror.obj',
				name: 'mirror'
			},
			{
				url: '../../3Dassets/chair.obj',
				name: 'chair'
			},
			{
				url: '../../3Dassets/picture.obj',
				name: 'picture'
			}
		])

		this.initGround();
		this.initWalls();
		this.initLight();



	}

	initGround() {
		var geometry = new THREE.PlaneGeometry(1000, 1000, 1, 1);
		var material = new THREE.MeshPhongMaterial({color: 0x663232});
		this._ground = new THREE.Mesh(geometry, material);
		this._ground.receiveShadow = true;
		this._ground.rotation.x = -Math.PI/2;
		this._ground.position.y = 0;
		this.add(this._ground);
	}

	initWalls() {
		var geometry = new THREE.PlaneGeometry(1000, 1000, 1, 1);
		var material = new THREE.MeshPhongMaterial({color: 0x8888ff});
		for(var i = 0; i < 4; i += 1) {
			this._walls.push(new THREE.Mesh(geometry, material));
			this._walls[i].receiveShadow = true;
			this.add(this._walls[i])
		}
		this._walls[0].position.x = -this._wallDistance;
		this._walls[0].rotation.y = Math.PI/2;
		this._walls[1].position.x = this._wallDistance;
		this._walls[1].rotation.y = -Math.PI/2;
		this._walls[2].position.z = this._wallDistance;
		this._walls[2].rotation.y = Math.PI;
		this._walls[3].position.z = -this._wallDistance;
	}

	initLight() {
		// const ambientLight = new THREE.AmbientLight(0x444444);
		// this._lights.push(ambientLight);
		// this.add(ambientLight);

		this.spotLight = new THREE.SpotLight(0xffffff, 1);
		this.spotLight.position.set( 160, 290, 30 );
		this.spotLight.castShadow = true;

		this.spotLight.distance = 2500;
		this.spotLight.decay = 2;
		this.spotLight.penumbra = 0;
		// this.spotLight.power = Math.PI/64;


		this.spotLight.shadow.mapSize.width = 8048;
		this.spotLight.shadow.mapSize.height = 8048;
		this.spotLight.shadowMapDarkness = 0.75;
		this.spotLight.shadow.camera.near = 1;
		this.spotLight.shadow.camera.far = 1000;


		this._lights.push(this.spotLight);
		this.add(this.spotLight);
	}

	dimLight() {
		if(this.spotLight.power > 0) {
			this.spotLight.power -= Math.PI/4;
			requestAnimationFrame(this.dimLight.bind(this));
		}
		else {
			return;
		}
	}

	increaseLight() {
		if(this.spotLight.power < Math.PI) {
			this.spotLight.power += Math.PI/4;
			requestAnimationFrame(this.increaseLight.bind(this));
		}
		else {
			return;
		}
	}

	initContent() {

		var objmng = this.objectManager;

		this._door = new SceneObject(false, false, false);
		this._door.addObject(objmng.getObjectsByName('door')[0].object);
		this.add(this._door);
		this._items.push(this._door);
		this._door.position.z = -this._wallDistance;

		this._shelf = new SceneObject(false, false, false);
		this._shelf.addObject(objmng.getObjectsByName('shelf')[0].object);
		this.add(this._shelf);
		this._items.push(this._shelf);
		this._shelf.position.set(160, 0, -240);

		this._wheels = new SceneObject(false, false, false);
		this._wheels.addObject(objmng.getObjectsByName('wheels')[0].object);
		this.add(this._wheels);
		this._items.push(this._wheels);
		this._wheels.position.set(235, 20, -220);

		this._switch = new SceneObject(false, false, false);
		this._switch.addObject(objmng.getObjectsByName('switch')[0].object);
		this.add(this._switch);
		this._items.push(this._switch);
		this._switch.position.set(80, 110, -260);

		this._chair = new SceneObject(false, false, false);
		this._chair.addObject(objmng.getObjectsByName('chair')[0].object);
		this.add(this._chair);
		this._items.push(this._chair);

		this._table = new SceneObject(false, false, false);
		this._table.addObject(objmng.getObjectsByName('table')[0].object);
		this.add(this._table);
		this._items.push(this._table);
		this._table.position.set(-120, 0, -120);
		this._table.rotation.set(0, Math.PI/3, 0);

		this._key = new SceneObject(false, false, false);
		this._key.addObject(objmng.getObjectsByName('key')[0].object);
		this.add(this._key);
		this._items.push(this._key);
		this._key.position.set(-120, 70, -160);

		this._picture = new SceneObject(false, false, false);
		this._picture.addObject(objmng.getObjectsByName('picture')[0].object);
		this.add(this._picture);
		this._items.push(this._picture);
		this._picture.position.set(-130, 70, -130);

		this._tape = new SceneObject(false, false, false);
		this._tape.addObject(objmng.getObjectsByName('tape')[0].object);
		this.add(this._tape);
		this._items.push(this._tape);
		this._tape.position.set(-120, 70, -100);

		this._taperecorder = new SceneObject(false, false, false);
		this._taperecorder.addObject(objmng.getObjectsByName('taperecorder')[0].object);
		this.add(this._taperecorder);
		this._items.push(this._taperecorder);
		this._taperecorder.position.set(-120, 70, -70);

		this._mirror = new SceneObject(false, false, false);
		this._mirror.addObject(objmng.getObjectsByName('mirror')[0].object);
		this.add(this._mirror);
		this._items.push(this._mirror);
		this._mirror.position.set(0, 150, 260);

	}

}
export default Room
