var THREE = require('three');

class SceneObject extends THREE.Object3D {

	constructor( bCollisionable, bInteractable, bMovable ) {
		super();
		this._isCollisionable = bCollisionable || false;
		this._isInteractable = bInteractable || false;
		this._isMovable = bMovable || false;
		this._object;
	}


	setCollisionable( bCollisionable ) {
		this._isCollisionable = bCollisionable;
	}
	setInteractable( bInteractable ) {
		this._isInteractable = bInteractable;
	}
	setMovable( bMovable ) {
		this._isMovable = bMovable;
	}

	addObject(object) {
		this._object = object;
		this.add(this._object);
	}

	update() {
		if(this._isMovable && this.position.y > 0) {
			this.position.y -= 0.1;
		}
	}

}


export default SceneObject;
