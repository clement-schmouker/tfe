var THREE = require('three');

class Creature extends THREE.Object3D {

	constructor() {
		super();
		this._light;
		this._mesh;
		this._bInspire = false;
		this._startingPosition;
		this._breathingSpeed = 0;
		this._name = "demo";
	}


	initialize() {
		this.initMesh();
		this.initLight();
		this._startingPosition = new THREE.Vector3(0, 100, -150);
		this.position.set(
			this._startingPosition.x,
			this._startingPosition.y,
			this._startingPosition.z
		);
	}

	initLight() {
		this._light = new THREE.PointLight(0xffffff, .4, 150, .01);
		this._light.castShadow = true;
		this._light.position.set(0, 10, 0);
		this.add(this._light);
	}

	initMesh() {
		var geometry = new THREE.SphereGeometry(5);
		var material = new THREE.MeshLambertMaterial({color: 0xff0000});
		this._mesh = new THREE.Mesh(geometry, material);
		this.add(this._mesh);
	}

	breathingAnimation() {

		var increase = Math.PI * 2 / 150;

		this.position.y = this._startingPosition.y + ( 7 * Math.sin( this._breathingSpeed ) / 2 );
		this._breathingSpeed += increase;

		// console.log(this.position.y);

	}

	update() {
		this.breathingAnimation();
	}

}

export default Creature;
