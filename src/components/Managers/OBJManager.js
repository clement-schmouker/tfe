var THREE = require('three'); //import tous les exports de three, les store dans un objet THREE, depuis le fichier "three"
var OBJLoader = require('three-obj-loader');

class OBJManager{

	constructor() {
		this.loaders = [];
	}


	onQueueLoaded() {

	}

	loadObject(objUrl,name){

		var _instance = this;
		var _loader = this.createLoader.call(this);
		_loader.url = objUrl;
		_loader.name = name || 'obj'+this.loaders.length;
		this.loaders.push(_loader);
		_loader.loader.load(objUrl,function(object){
			object.scale.set(30, 30, 30);
			_loader.object = object;
			console.log(_loader.name+" loaded !");
			_loader.loaded = true;
			_instance.notifyComplete()
		})


	};

	loadObjects(objsToloadFromArray){

		objsToloadFromArray.map(function (obj){

			this.loadObject(obj.url,obj.name)

		},this)

	};

	notifyComplete(){
		var loaded = this.loaders.filter(function (loader){

			return loader.loaded

		})
		if (loaded.length >= this.loaders.length)this.onQueueLoaded.call(this,this.loaders)

	}

	getObjectsByName(name){

		return this.loaders.filter(function (loader){

			return loader.name == name

		})

	}

	getObjectsByUrl(url){

		return this.loaders.filter(function (loader){

			return loader.url == url

		})

	}

	cloneObject(name){

		var _array = this.getObjectsByName(name);
		if (_array.length<=0)return null
		return Object.create({},_array[0].object)

	}

	createLoader (){

		return {

			loader:new THREE.OBJLoader(),
			loaded:false,
			object:null,
			url:'',
			name:''

		}

	}

}

export default OBJManager
