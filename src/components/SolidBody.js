const THREE = require('three'); //import tous les exports de three, les store dans un objet THREE, depuis le fichier "three"


class SolidBody extends THREE.Object3D {

	constructor() {
		super();
		this.isInteractable = false;
		this.isMovable = false;
		this._mesh
	}

	update() {

	}

	setInteractable() {
		this.isInteractable = true;
	}

	setMovable() {
		this.isMovable = true;
	}

}


export default SolidBody
