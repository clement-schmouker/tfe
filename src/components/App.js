/**
  * App Manager
  *
  * @memberOfApp
  */

//IMPORTS
const THREE = require('three'); //import tous les exports de three, les store dans un objet THREE, depuis le fichier "three"
var OBJLoader = require('three-obj-loader');
OBJLoader(THREE);
import CustomCube from 'Cube';
import Room from 'Room';
import Creature from './entities/Creature';
import SceneObject from './entities/SceneObject';
const OrbitControls = require('three-orbit-controls')(THREE)
const { Stats } = require('three-stats');
import debounce from 'lodash/debounce';


class App {

	// this.name = "";

	constructor() {
        this._lights = [];
        this._objects = [];
		this._rooms = [];
		this._creature;
		this.initScene();
		this._camera.lookAt(0, 0, 0);

	}

	initScene() {
		console.log('Scene initialization');

		this._width = window.innerWidth;
		this._height = window.innerHeight;
		this._scene = new THREE.Scene();
        if(global.debug) {
            window.scene = this._scene;
            window.THREE = THREE;
        }

		this.initHead();
        this.initCamera();

        this.initRenderer();

		//rooms
		this.initRoom();

        //add keys
        this.initKeys();

		//load scene object
		// this.loadObjects();

        //debug mode
        if(global.debug) {
            this.initControls();
            this.initStats();
			this.addHelpers();
        }


		this.initCreature();
        //launch app rendering loop
        this.renderApp();
		this.bind();

		console.log('scene initialized');
	}

	initRoom() {
		var room = new Room();
		room.initialize();

		this._rooms.push(room);
		this._scene.add(room);
	}

	// loadObjects() {
	// 	var loadedObjectScale = 30;
	// 	var loader = new THREE.OBJLoader();
	// 	loader.load( '../../3Dassets/scene.obj', function ( object ) {
	// 		object.scale.set(loadedObjectScale, loadedObjectScale, loadedObjectScale);
	// 		for(var i = 0; i < object.children.length; i += 1 ){
	// 			object.children[i].castShadow = true;
	// 			object.children[i].receiveShadow = true;
	// 		}
	// 	  scene.add( object );
	// 	} );
	// }


	initHead() {
		this._playerHead = new THREE.Object3D();
		this._playerHead.position.z = 0;
		this._playerHead.position.y = 125;
		this._scene.add(this._playerHead);
	}


	initCamera() {
		const fieldOfView = 60;
		const aspectRatio = this._width / this._height;
		const nearPlane = 1;
		const farPlane = 2000;

		this._camera = new THREE.PerspectiveCamera(
			fieldOfView,
			aspectRatio,
			nearPlane,
			farPlane
		);
		this._playerHead.add(this._camera);
	}


	initRenderer() {
		this._renderer = new THREE.WebGLRenderer({
            antialias: true
        });

		this._renderer.setSize(this._width, this._height);
        this._renderer.shadowMap.enabled = true;
		document.body.appendChild(this._renderer.domElement);

	}


    initControls() {
        this._controls = new OrbitControls(this._camera, this._renderer.domElement);
    }


    initStats() {
        this._stats = new Stats();
        this._stats.showPanel(0);
        document.body.appendChild(this._stats.dom);
    }


	updateApp() {
        this._objects.forEach((obj) => {
            obj.update();
        })
		this._creature.update();

		this._rooms[0]._items.forEach((obj) => {
			obj.update();
		})
		//  this._rooms[0].door.object.position.z += 0.3;
		//  this._rooms[0].door.children[1].rotation.set(0, 0, Math.PI/2);
				// console.log(this.door.children);
	}

    bind() {
        window.addEventListener('resize', debounce(this.onResize.bind(this), 500))
    }

    onResize() {
        this._width = window.innerWidth;
        this._height = window.innerHeight;
        this._renderer.setSize(this._width, this._height);
        this._camera.aspect = this._width / this._height;
        this._camera.updateProjectionMatrix();
    }

	initCreature() {
		this._creature = new Creature();
		this._creature.initialize();
		this._scene.add(this._creature);
		console.log(this._creature);
	}

    addHelpers() {
        //go through lights
        this._rooms[0]._lights.forEach((item) => {
            if(item.shadow) {
                this.addHelper(item.shadow.camera);
            }
        })
        //Add helpers
    }

    addHelper(camera) {
        const helper = new THREE.CameraHelper(camera)
        this._scene.add(helper);
    }


    initKeys() {
        this.pressedKey = [];

        addEventListener('keydown', (event) => {
            // console.log(event.key, event.keyCode);
            switch(event.key) {
                case 'z':
                    this.pressedKey[0] = true;
                    break;
                case 's':
                    this.pressedKey[1] = true;
                    break;
                case 'q':
                    this.pressedKey[2] = true;
                    break;
                case 'd':
                    this.pressedKey[3] = true;
                    break;
                case 'a':
                    this.pressedKey[4] = true;
                    break;
                case 'e':
                    this.pressedKey[5] = true;
                    break;


				case 'ArrowUp':
					this.pressedKey[6] = true;
					break;
				case 'ArrowDown':
					this.pressedKey[7] = true;
					break;
				case 'ArrowLeft':
					this.pressedKey[8] = true;
					break;
				case 'ArrowRight':
					this.pressedKey[9] = true;
					break;
            };

        })

        addEventListener('keyup', (event) => {
            switch(event.key) {
                case 'z':
                    this.pressedKey[0] = false;
                    break;
                case 's':
                    this.pressedKey[1] = false;
                    break;
                case 'q':
                    this.pressedKey[2] = false;
                    break;
                case 'd':
                    this.pressedKey[3] = false;
                    break;
                case 'a':
                    this.pressedKey[4] = false;
                    break;
                case 'e':
                    this.pressedKey[5] = false;
                    break;


				case 'ArrowUp':
					this.pressedKey[6] = false;
					break;
				case 'ArrowDown':
					this.pressedKey[7] = false;
					break;
				case 'ArrowLeft':
					this.pressedKey[8] = false;
					break;
				case 'ArrowRight':
					this.pressedKey[9] = false;
					break;
            };

        })

    }

    updateMovement() {
        const angle = this._cube.rotation.y;

        //rotate
        (this.pressedKey[4] ? this._cube.rotation.y += this._cube.speed / 100 : false);
        (this.pressedKey[5] ? this._cube.rotation.y -= this._cube.speed / 100 : false);

        //deplacement
        if(this.pressedKey[0] ){
            this._cube.position.z += this._cube.speed* Math.cos(angle);
            this._cube.position.x += this._cube.speed* Math.sin(angle);
            this._cube._mesh.rotation.x += .1;
        }
        if(this.pressedKey[1]) {
            this._cube.position.z -= this._cube.speed* Math.cos(angle);
            this._cube.position.x -= this._cube.speed* Math.sin(angle);
            this._cube._mesh.rotation.x -= .1;
        }
        if(this.pressedKey[2]) {
            this._cube.position.z += this._cube.speed* Math.sin(-angle);
            this._cube.position.x += this._cube.speed* Math.cos(angle);
            // this._cube._mesh.rotation.z -= .1;
        }
        if(this.pressedKey[3]) {
            this._cube.position.z -= this._cube.speed* Math.sin(-angle);
            this._cube.position.x -= this._cube.speed* Math.cos(angle);
            // this._cube._mesh.rotation.z += .1;
        }

    }

	moveHead() {

		if(this.pressedKey[0] && this._camera.rotation.x < Math.PI/3){
			this._camera.rotation.x += 0.05;
			console.log("z");
		}
		if(this.pressedKey[1] && this._camera.rotation.x > - Math.PI/3) {
			this._camera.rotation.x -= 0.05;
			console.log("s");
		}
		if(this.pressedKey[2]) {
			this._playerHead.rotation.y += 0.05;
			console.log("q");
		}
		if(this.pressedKey[3]) {
			this._playerHead.rotation.y -= 0.05;
			console.log("d");
		}

		if(this.pressedKey[4]) {
			this._rooms[0].increaseLight();
		}
		if(this.pressedKey[5]) {
			this._rooms[0].dimLight();
		}

		if(this.pressedKey[6]) {
			this._creature.position.z -= 1;
		}
		if(this.pressedKey[7]) {
			this._creature.position.z += 1;
		}
		if(this.pressedKey[8]) {
			this._creature.position.x -= 1;
		}
		if(this.pressedKey[9]) {
			this._creature.position.x += 1;
		}
	}


	renderApp() {
        if(this._stats) {
            this._stats.begin();
        }

		this._renderer.render(this._scene, this._camera);
		requestAnimationFrame(() => {
            this.renderApp();
            this.updateApp();
			this.moveHead();
        }); // équivalent à un .bind(this);

        if(this._stats) {
            this._stats.end();
        }
    }

    add(obj) {
        this._objects.push(obj);
        this._scene.add(obj);
    }
}


export default App;
